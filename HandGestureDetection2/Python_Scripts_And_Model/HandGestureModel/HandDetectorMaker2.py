"""
HandDetectorMaker.py
A single Unit designed to deploy on AWS and check the performance benchmark.
Can't recieve and send data over socket but prediction is active and have been benchmarked against multiple datasets
which are freely available online.
"""
import os

import DataLoader as DataLoader  # Custom data loader script
import EasySocket as EasySocket  # Custom scoket script
import NetLoader as NetLoader  # Custom net loader script
import warnings
import numpy as np

import pandas as pd
from PIL import Image

from tensorflow.keras import models, layers, optimizers
from tensorflow.keras.applications import VGG16
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.preprocessing import image as image_utils
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.utils import to_categorical
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
import DataLoader as DataLoader
# Custom data loader script

imageSize=50

train_mode = False

image_val_data_location_1_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Surya2/"
image_val_data_location_2_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Varuna2/"
image_val_data_location_3_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Vayu2/"
image_val_data_location_4_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Aakash2/"
image_val_data_location_5_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Chatura2/"
image_val_data_location_6_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Apan2/"
image_val_data_location_7_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Gyaan2/"
image_val_data_location_8_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/None/"
image_val_data_location_9_1 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Abhay2/"
image_val_data_location_1_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Surya/"
image_val_data_location_2_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Varuna/"
image_val_data_location_3_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Vayu/"
image_val_data_location_4_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Aakash/"
image_val_data_location_5_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Chatura/"
image_val_data_location_6_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Apan/"
image_val_data_location_7_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Gyaan/"
image_val_data_location_8_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/None2/"
image_val_data_location_9_2 ="/home/ankit/gesturerecognition/HandGestureDetection2/HandGestureData/Abhay/"

res_x =50
res_y =50

# MUST BE THE SAME LOCATION AS THE PATH IN HandGesture.java
real_time_path = "/Users/NC914/gestureRecognition/HandGestureDetection2/HandGestureData/real_time.png"

raw_input_size =0
# there are no real input so set to 0
raw_output_size =9

data_list = []

        
data = DataLoader.DataLoader(data_list, size_x=res_x, size_y=res_y, num_inputs=raw_input_size, num_outputs=raw_output_size, black_white=False)

vgg_base = VGG16(weights='imagenet', include_top=False, input_shape=(3,res_x, res_y))
optimizer1 = optimizers.Adam()

base_model = vgg_base
# Topless
# Add top layer
x = base_model.output
x = Flatten()(x)
x = Dense(128,activation='relu',name='fc1')(x)
x = Dense(128,activation='relu',name='fc2')(x)
x = Dense(128,activation='relu',name='fc3')(x)
x = Dropout(0.5)(x)
x = Dense(64,activation='relu',name='fc4')(x)
predictions = Dense(9, activation='softmax')(x)


model = Model(inputs=base_model.input, outputs=predictions)
for layer in base_model.layers:
        layer.trainable =False
model.compile(loss='categorical_crossentropy',optimizer=optimizers.Adam(),metrics=['accuracy'])

callbacks_list = [EarlyStopping(monitor='val_acc', patience=3, verbose=1)]

model.compile(optimizer='Adam',loss='categorical_crossentropy',metrics=['accuracy'])


if(train_mode == True):
        data.combine_data(random_sort=True)
        input_element_1, output_element_1= data.get_set_elements_to_train(0)

        file_path ='./models/saved_model.hdf5'
        model_checkpoint = ModelCheckpoint(filepath=file_path, save_best_only=True)

        early_stopping = EarlyStopping(monitor='val_acc',min_delta=0,patience=10,verbose=1,mode='auto',restore_best_weights=True)

        model.fit(input_element_1[0], output_element_1,epochs=200,batch_size=64,validation_split=0.2,verbose=1,callbacks=[early_stopping, model_checkpoint])
else:
    socket = EasySocket.EasySocket(preset_unpack_types=['i'])  # add a preset type of 1 integer (get that float value)

    socket.connect()  # connect to server

    while True:

        if (socket.get_anything(4, 0) == True):
            in_image=[]
            raw_RGB=raw_RGB = data.load_image(real_time_path)
            in_image.append(raw_RGB)
            in_image = np.array(in_image, dtype = np.float32)

            file_path ='./models/saved_model.hdf5'

            model.load_weights(file_path)

            result=model.predict(in_image)

            print( result)

            # create a line of message
            message = ""
            for i in range(0, len(result[0])):
                message += str(result[0][i])
                if (i == len(result[0]) - 1):
                    message += "\n"
                else:
                    message += " "

            socket.send_string_data(message)  # send prediction message to server

